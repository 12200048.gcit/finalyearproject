@props(['messages'])

@if ($messages)
<div {{ $attributes->merge(['class' => 'text-danger text-start']) }}>
    @foreach ((array) $messages as $message)
    <p>{{ $message }}</p>
    @endforeach
</div>
@endif