<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/homepage.css')}}">
    <link rel="stylesheet" href="{{asset('css/booking.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <style>
        .nav-link.active {
            color:#0056b3 !important;
            text-decoration: underline
        }
        .nav-link:hover{
            text-decoration: underline;
            color:#0056b3
        }

        .avatar {
            width: 30px; /* Adjust the size as needed */
            height: 30px; /* Adjust the size as needed */
            background-color: #2764AC; /* Adjust the color as needed */
            color: white ;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 20px; /* Adjust the font size as needed */
            text-transform: uppercase;
            font-weight: bold; 
            text-decoration: none;
        }
    </style>
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <img class="rlogo" src="{{asset('img/logo.png')}}" /> 
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            
            
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'user.homepage' ? 'active' : 'homepage' }}" href="{{route('user.homepage')}}">FACILITIES</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'user.mybooking' ? 'active' : 'mybooking' }}" href="{{route('user.mybooking')}}">MY BOOKINGS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'user.feedback' ? 'active' : 'feedback' }}" href="{{route('user.feedback')}}">FEEDBACKS</a>
            </li>
            
            
            </ul>
            {{-- <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" width="30px" height="30" type="submit">
                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M11 2a9 9 0 1 0 5.618 16.032l3.675 3.675a1 1 0 0 0 1.414-1.414l-3.675-3.675A9 9 0 0 0 11 2m-6 9a6 6 0 1 1 12 0a6 6 0 0 1-12 0" clip-rule="evenodd"/></svg>
            </button>
            </form> --}}
            {{-- <div class="d-flex">
            <svg cursor="pointer" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24"><path fill="currentColor" d="M4 19v-2h2v-7q0-2.075 1.25-3.687T10.5 4.2v-.7q0-.625.438-1.062T12 2t1.063.438T13.5 3.5v.7q2 .5 3.25 2.113T18 10v7h2v2zm8 3q-.825 0-1.412-.587T10 20h4q0 .825-.587 1.413T12 22m-4-5h8v-7q0-1.65-1.175-2.825T12 6T9.175 7.175T8 10z"/></svg>
            <div class="flex items-center"> --}}
                <div class="flex items-center ms-3">
                  <div>
                    <button style="margin-right: 55px" type="button" class="flex text-sm rounded-full focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-600" aria-expanded="false" data-dropdown-toggle="dropdown-user">
                      <span class="sr-only">Open user menu</span>
                      <div class="avatar">
                        {{ substr(Auth::user()->name, 0, 1) }}
                    </div>
                    </button>
                  </div>
                  <div class="z-50 hidden my-4 text-base list-none bg-white divide-y divide-gray-100 rounded shadow dark:bg-gray-700 dark:divide-gray-600" id="dropdown-user">
                    <ul class="py-1" role="none">
                      <li>
                        {{-- <a href="{{route('user.profile')}}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-white" role="menuitem">Profile</a> --}}
                        <form action="{{route('user.profile')}}" method="get" style="margin: 0;">
                            <button type="submit" class="btn btn-primary" style="width: 100%; margin-left:-15px">Profile</button>
                          </form>
                      </li>
                    </ul>
                    <ul class="py-1" role="none">
                        <li>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button type="submit" class="btn btn-primary" style="width:100%; padding-right:30px; margin-left:-15px">Logout</button>
                            </form> 
                        </li>
                      </ul>
                  </div>
                </div>
              </div>
          </div>
        </div>

        </div>
        </div>
    </nav>
<main>
    {{$slot}}
</main>
    
   

<footer>
    <div class="footer-container">
        <div class="footer-logo">
            <img src="{{asset('img/logo.png')}}" alt="Sherubtse College Logo">
        </div>
        <div class="footer-section">
            <p>Sherubtse College</p>
            <p>Kanglung, Trashigang</p>
            <p><a href="mailto:sherubtse@rub.edu.bt">sherubtse@rub.edu.bt</a></p>
            <p>+975-1234567</p>
        </div>
        
        <div class="footer-socialLinks">
            <h4>Follow Us</h4>
            <a href="#"><i class="fa-brands fa-facebook"></i></a>
            <a href="#"><i class="fa-brands fa-youtube"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
        </div>
    </div>
    <hr class="footer-separator">
    <p>COPYRIGHTS SHERUBTSE COLLEGE 2024</p>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="js\home.js"><script/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>