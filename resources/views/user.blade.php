<x-app-layout>
    <style>
        .btn-outline-primary {
        padding: 5px 12px; /* Adjust padding to make it smaller */
        font-size: 12px; /* Decrease font size */
        margin-top: 15px;
    }

        .btn-outline-primary:hover {
            color: #fff; /* Change text color to white */
            background-color: #dc3545; /* Change background color to red */
            border-color: #dc3545; /* Change border color to red */  
        }
        
    </style>

    <link rel="stylesheet" href="{{asset('../../../assets/css/booking.css')}}">
    <h1>Users</h1>
    <div class="maincontainer">
        <div class="row">
            <div class="d-flex">
                <div class="p-2 ms-auto">
                    <form id="userTypeForm" action="{{route('users')}}" method="GET">
                        @csrf
                        <select style="height: 31px; border-radius: 5px" id="userType" name="userType">
                            <option>Select Role</option>
                            <option value="student">Student</option>
                            <option value="staff">Staff</option>
                        </select>
                    </form>
                </div>
                <div class="p-2">
                    <div class="input-group input-group mb-3">
                        <input  id="searchInput" type="text" class="form-control border-dark" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Search">
                        <span class="input-group-text border-dark" id="inputGroup-sizing-sm"><i class="fas fa-search"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class=" table-responsive p-3">
            <table id="facilityTable" class="searchable table-info w-100">
                <thead>
                    <tr>
                        <th scope="col" style="color:white; padding-Left:5px">Sl.no</th>
                        <th id="headerLabel" scope="col" style="color:white; padding-Left:5px">Student ID</th>

                        <th scope="col" style="color:white; padding-Left:5px">Username</th>

                        <th scope="col" style="color:white; padding-Left:5px">E-mail</th>
                        <th scope="col" style="color:white; padding-Left:5px">Contact.no</th>

                        <th scope="col" style="color:white; padding-Left:5px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $serialNumber = 1; // Initialize the serial number counter
                    @endphp
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{$serialNumber}}</th>
                        <td style=" font-size:12px; padding-Left:5px">{{$user->employeeid}}</td>
                        <td style="font-size:12px; padding-Left:5px">{{$user->name}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$user->email}}</td>
                        <td style="font-size:12px;padding-Left:5px">{{$user->contact}}</td>

                        <td>
                            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                Delete
                            </button>

                            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="staticBackdropLabel">Delete Users</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to delete this user?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <form action="{{ route('user.deleteUser', $user->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-secondary">Yes</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                    @php
                        $serialNumber++; // Increment the serial number for the next iteration
                    @endphp
                    @endforeach

                </tbody>
            </table>
            <script>
                // Get the input field and table
                var input = document.getElementById("searchInput");
                var table = document.getElementById("facilityTable");
                var rows = table.getElementsByTagName("tr");
            
                // Add event listener to the input field
                input.addEventListener("input", function() {
                    var filter = input.value.toUpperCase();
            
                    // Loop through all table rows, and hide those who don't match the search query
                    for (var i = 0; i < rows.length; i++) {
                        var facilityNameColumn = rows[i].getElementsByTagName("td")[2]; // Adjust index if needed
                        if (facilityNameColumn) {
                            var textValue = facilityNameColumn.textContent || facilityNameColumn.innerText;
                            if (textValue.toUpperCase().indexOf(filter) > -1) {
                                rows[i].style.display = "";
                            } else {
                                rows[i].style.display = "none";
                            }
                        }
                    }
                });
            </script>
            <p style="text-align:center">
                << @if ($users->lastPage() > 1)
                    @for ($i = 1; $i <= $users->lastPage(); $i++)
                        <a style="margin-inline: 10px" href="{{ $users->url($i) }}">{{ $i }}</a>
                    @endfor
                @endif >>
            </p>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        function changeTableHeader(option) {
            const headerLabel = document.getElementById('headerLabel');
            if (option === 'Student') {
                headerLabel.textContent = 'Student ID';
            } else if (option === 'Staff') {
                headerLabel.textContent = 'Staff ID';
            }
        }
    </script>
    <script>
        $(document).ready(function() {
            $('#userType').on('change', function() {
                $('#userTypeForm').submit();
            });
        });   
    </script>
</x-app-layout>